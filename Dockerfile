FROM python:3.6-alpine

ADD . /app
WORKDIR /app

RUN pip install tox
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install --no-cache-dir -r requirements_dev.txt