# Jackdaw IPReg Python API Client Library

Jackdaw Ipreg documentation - https://www.dns.cam.ac.uk/ipreg/api/

# Design

The Jackdaw IPReg API is a very basic, very limited API that has been bolted on to a regular CRUD web application
and tends to follow UI conventions rather than API conventions.

## Jackdaw ipreg Overview

* list_ops - read-only actions:
  * list_domain
  * list_lan
  * list_mzone
  * list_subnet
* xlist_ops - read-only actions:
  * list_domain
  * list_mzone

* list_ops and xlist_ops read-write actions:
  * modify
  * register
  * rename
  * rescind

## Function Overview

There are two levels of function calls exposed via the API. 
1) Direct mapping to ipreg list_ops and xlist_ops
2) Convenience methods that abstract functionality on methods from 1 - often you'll want to use these because 
they more closely represent a data structure you are interested in. A cname, an ip registration, etc.

### Direct Ipreg functions

All the basic read functions are available in the ipreg library.

The write functions are not yet implemented.

Underneath the hood there is an endpoint object that maps these calls into HTTP requests
using the appropriate post data.

i.e.

```
action_data = {
    'do_it': 'list_subnet',
    'subnet_base': subnet,
    'record_separator': 'CRLF'
}
```

#### list_ops

`get_list_ops_by_domain(domain)`

`get_list_ops_by_subnet(subnet)`

`get_list_ops_by_mzone(mzone)`

`get_list_ops_by_lan(lan)`

#### xlist_ops

There are two xlist_ops calls which vary by domain or mzone.

`get_xlist_ops_by_domain(domain, object_type='box')` 

`get_xlist_ops_by_mzone(mzone, object_type='box')`

Within each of these calls you can specify which xlist_op object
you want to work on.

The choices for `object_type` are:

* box - a computer, piece of equipment, switch, server, etc.
* vbox - a virtual one of the above
* cname - a DNS cname 
* aname - a DNS a record

### Convenience Methods

As the API doesn't expose any single_ops functionality, we take advantage of using the direct methods to 
abstract out these calls.

`get_ip_info(self, ip)` - Retrieves info on a single IP by retrieving all the entries for a subnet and searching
for a matching entry.

`get_hostname_info(self, hostname)` - Retrieves info on a single domain by retrieving all entires for a domain and
searching for a matching entry.

The convenience methods will expand to cover most of the functionality in `single_ops`.

## Caching

As the API only exposes the bulk operators list_ops and xlist_ops we don't want to hammer the API for
running lots of pseudo-single-ops functions like `get_ip_info`.

The library is designed to cache the calls from get_xlist*, get_list* and use those for subsequent
single access calls. The default cache time is set to 180 seconds. This means for each unique
API call the system will only hit the ipreg server once every 180 seconds.

Example:

You want to do lots of searchings on the subnets 192.168.0.1 and 192.168.20.1. 
You write your script to do single_ops searches like:
```
get_ip_info('192.168.0.12)
get_ip_info('192.168.0.45)
get_ip_info('192.168.0.43)
get_ip_info('192.168.20.100)
get_ip_info('192.168.0.12)
get_ip_info('192.168.20.124)
get_ip_info('192.168.20.122)
get_ip_info('192.168.0.100)
``` 

For those 8 calls to get_ip_info only two API calls will be made. The first reference to a new
subnet will trigger a HTTP get. Each subsequent call will use the local cache until the 180 seconds have expired.

The cache time can be set when instantiating the API but it won't let you set a time shorter than 5 seconds to
be nice to our data providers.

# Setup

TODO: Complete setup

## Cookies

Jackdaw ipreg provides access via downloading a long-term cookie.

Cookie file - https://www.dns.cam.ac.uk/ipreg/api/

Download cookie - https://jackdaw.cam.ac.uk/ipreg/download-cookie

# Usage

```
>>> import ipreg
>>> api_client = ipreg.IPRegApi(cookie_file='cookie-jackdaw-dev.txt')
>>> api_client.get_list_ops_by_subnet('192.168.1.0')
[{'address': '192.168.1.15', 'mzone': 'MYMZONE', 'lan': 'MYLAND', 'name': 'comp58.mydomain.test.cam.ac.uk' ...},{'address': '192.168.1.27', 'mzone': 'MYMZONE', 'lan': 'MYLAND', 'name': 'comp66.mydomain.test.cam.ac.uk' ...}]
>>> api_client.get_ip_info('192.168.1.15')
{'address': '131.111.92.166', 'mzone': 'MYMZONE', 'lan': 'MYLAND', 'name': 'comp58.mydomain.test.cam.ac.uk'...
```

The previous example shows an interactive session that pulls back a whole subnet info and then a specific
ip address detail.

```python
import ipreg
api_client = ipreg.IPRegApi(cookie_file='/path/to/my/cookie-jackdaw.txt', cache_dir='/path/to/my/cache/')

fqdn = 'comp58.mydomain.test.cam.ac.uk'
ip = '192.168.1.15'

ip_info = api_client.get_ip_info(ip)
hostname_info = api_client.get_hostname_info(fqdn)
```

# Testing

_TODO: Outline how the pytest fixtures load mock vs prod test data in JSON._

The project is tested with pytest using tox test runners. Check out the .gitlab-ci.yml file for info on the
automated test runners.

This project makes use of pytest fixtures and the requests_mock to inject mocked URL responses. 
The mocked responses are in a subdirectory of the testing folder.

The production tests are not stored in this project as they are specific to each user and should only
contain limited read tests if you don't have access to a test instance.

## Running Mock Tests 

These can be run directly from gitlab instance or manually.

From within the `tests` directory, run pytest

`pytest --source mock --test_data mock-tests.json`

## Running Prod Tests

For production tests you need to specify a different set of test data, a cookie file and specify
that they are production tests.

`pytest --source prod --test_data prod-tests.json --cookie_file ../cookie-jackdaw-dev.txt`

## Manual Testing

You can manually run the tox tests by building the local docker container and running the test suites.

```
docker build -t cscs/jackdaw-test-image .
docker run -t cscs/jackdaw-test-image tox -e py36
docker run -t cscs/jackdaw-test-image tox -e flake8
```
