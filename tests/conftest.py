import pytest
import os
import tests.mock_data
import json


class IpregEnvironment:

    def __init(self):
        self.type = None
        self.cookie_file_name = None


@pytest.fixture
def ipreg_mock_environment(requests_mock):

    # The testing cache directory needs to exist
    current_directory = os.getcwd()
    final_directory = os.path.join(current_directory, r'cache-tests')
    pytest.cache_directory = final_directory
    if not os.path.exists(final_directory):
        os.makedirs(final_directory)

    # A fake cookie is needed to ensure that the cookie validation
    # part of the API will pass.
    cookie_file_name = os.path.join(final_directory, 'cookie-jackdaw-gen-test.txt')
    with open(cookie_file_name, 'w') as f:
        cookie_parts = [
            'domain.co.uk',
            'FALSE',
            '/',
            'TRUE',
            '1580000000',
            'Cookie_Name',
            'status&200&user&frank&token&atokenvalue'
        ]
        f.write("\t".join(cookie_parts))

    # Configure Environment
    environment = IpregEnvironment()
    environment.type = "mock"
    environment.cookie_file_name = cookie_file_name

    # Pre-load all mock URLs. This will be inefficient at scale so this may get refactored
    # when required.

    mock_ipreg_data = tests.mock_data.ipreg.IPReg()  # Generates jackdaw ipreg responses
    for function_call in pytest.test_data:
        for test_set in pytest.test_data[function_call]:

            # Register mock data against url
            params = test_set['params']
            data = mock_ipreg_data.load_data(function_call, params)
            requests_mock.get(data['url'], text=data['text_response'])

    return environment


@pytest.fixture
def ipreg_cookie_environment(request):

    cookie_file_name = request.config.getoption("--cookie_file")

    if cookie_file_name == "":
        current_directory = os.getcwd()
        up_string = ".." + os.path.sep
        filename = "cookie-jackdaw-dev.txt"
        cookie_file_name = os.path.abspath(os.path.join(current_directory, up_string, filename))

    # Configure environment

    environment = IpregEnvironment()
    environment.type = "cookie"
    environment.cookie_file_name = cookie_file_name

    return environment


@pytest.fixture
def ipreg_environment(request):

    source = request.config.getoption("--source")

    environment = None
    if source == 'prod':
        environment = request.getfixturevalue('ipreg_cookie_environment')
        return environment
    elif source == 'mock':
        environment = request.getfixturevalue('ipreg_mock_environment')
        return environment

    # Still to yield teardown
    # os.rmdir(pytest.cache_directory)
    # os.remove(pytest.cookie_file_name)

    raise Exception('Environment not specified')


@pytest.fixture()
def test_package(request):
    def make_test_package(test_files):

        return test_package

    return make_test_package


def pytest_addoption(parser):
    parser.addoption(
        "--source",
        action="store",
        default="",
        help="select source of tests",
    )
    parser.addoption(
        "--test_data",
        action="store",
        default="",
        help="select data source of tests",
    )
    parser.addoption(
        "--cookie_file",
        action="store",
        default="",
        help="select cookie file",
    )


def pytest_generate_tests(metafunc):
    if 'source' in metafunc.fixturenames:
        metafunc.parametrize('source', metafunc.config.getoption('source'))

    # Load the test case data
    pytest.test_data = {}
    test_data_file = metafunc.config.getoption('test_data')
    if test_data_file:
        with open(test_data_file) as f:
            pytest.test_data = json.load(f)

        # Load tests for list_ops
        for test_fixture_set in pytest.test_data:
            if test_fixture_set in metafunc.fixturenames:
                metafunc.parametrize(test_fixture_set, pytest.test_data[test_fixture_set])
