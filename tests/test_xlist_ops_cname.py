import requests
import requests_mock
import tests.mock_data

import os
import pytest

import ipreg


def test_get_xlist_ops_by_valid_mzone(ipreg_environment, xlist_ops_cname_by_mzone):

    mzone = xlist_ops_cname_by_mzone['params']['mzone']
    domain = xlist_ops_cname_by_mzone['params']['domain']
    expected_count = xlist_ops_cname_by_mzone['results']['expected_count']

    api_client = ipreg.IPRegApi(cookie_file=ipreg_environment.cookie_file_name)

    cname_items = api_client.get_xlist_ops_by_mzone(mzone, 'cname')

    # Check that each address that comes back from the API contains the start of the domain
    for cname in cname_items:
        assert domain in cname['name']
        assert domain in cname['target_name']

    assert len(cname_items) == expected_count
