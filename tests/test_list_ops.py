import requests
import requests_mock
import tests.mock_data

import os
import pytest

import ipreg


def test_get_list_ops_by_subnet_valid_subnet(ipreg_environment, list_ops_by_list_subnet):

    subnet = list_ops_by_list_subnet['params']['subnet']
    expected_count = list_ops_by_list_subnet['results']['expected_count']

    api_client = ipreg.IPRegApi(cookie_file=ipreg_environment.cookie_file_name)

    subnet_items = api_client.get_list_ops_by_subnet(subnet)

    # Check that each address that comes back from the API contains the start of the subnet
    check_subnet = subnet.rstrip(".0")
    for subnet_item in subnet_items:
        assert check_subnet in subnet_item['address']

    assert len(subnet_items) == expected_count


def test_get_list_ops_by_subnet_valid_domain(ipreg_environment, list_ops_by_list_domain):

    domain = list_ops_by_list_domain['params']['domain']
    expected_count = list_ops_by_list_domain['results']['expected_count']

    api_client = ipreg.IPRegApi(cookie_file=ipreg_environment.cookie_file_name)

    domain_items = api_client.get_list_ops_by_domain(domain)

    # Check that each address that comes back from the API contains the start of the subnet

    for domain_item in domain_items:
        assert domain in domain_item['name']

    assert len(domain_items) == expected_count


def test_get_list_ops_by_subnet_valid_mzone(ipreg_environment, list_ops_by_list_mzone):

    mzone = list_ops_by_list_mzone['params']['mzone']
    expected_count = list_ops_by_list_mzone['results']['expected_count']

    api_client = ipreg.IPRegApi(cookie_file=ipreg_environment.cookie_file_name)

    mzone_items = api_client.get_list_ops_by_mzone(mzone)

    # Check that each address that comes back from the API contains the start of the subnet

    for mzone_item in mzone_items:
        assert mzone == mzone_item['mzone']

    assert len(mzone_items) == expected_count


def test_get_list_ops_by_subnet_valid_lan(ipreg_environment, list_ops_by_list_lan):

    lan = list_ops_by_list_lan['params']['lan']
    expected_count = list_ops_by_list_lan['results']['expected_count']

    api_client = ipreg.IPRegApi(cookie_file=ipreg_environment.cookie_file_name)

    lan_items = api_client.get_list_ops_by_lan(lan)

    # Check that each address that comes back from the API contains the start of the subnet

    for lan_item in lan_items:
        assert lan == lan_item['lan']

    assert len(lan_items) == expected_count
