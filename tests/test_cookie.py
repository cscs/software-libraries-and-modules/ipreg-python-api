import ipreg
import pytest
import os


def test_cookie_file_does_not_exist():
    with pytest.raises(ipreg.exceptions.JackdawCookieNotFoundError) as excinfo:
        api_client = ipreg.IPRegApi(cookie_file='./non-existent-cookie-jackdaw-test.txt')

    assert "Cookie file not found" in str(excinfo.value)


def test_cookie_file_exists_but_invalid_data(ipreg_environment):
    bad_cookie_name = "bad-cookie-name.txt"
    with open(bad_cookie_name, 'w') as f:
        f.write("blank cookie")

    with pytest.raises(IndexError):
        api_client = ipreg.IPRegApi(cookie_file=bad_cookie_name)

    os.remove(bad_cookie_name)


def test_cookie_with_valid_format_but_bad_data():
    cookie_file_name = 'test-cookie.txt'
    with open(cookie_file_name, 'w') as f:
        cookie_parts = [
            'domain.co.uk',
            'FALSE',
            '/',
            'TRUE',
            '1580000000',
            'Cookie_Name',
            'status&200&user&frank&token&atokenvalue'
        ]
        f.write("\t".join(cookie_parts))

    api_client = ipreg.IPRegApi(cookie_file=cookie_file_name, cache_dir='cache-tests')
    os.remove(cookie_file_name)

# def test_cookie_is_valid():
#     """NOTE: You must provide a cookie for this test to pass"""
