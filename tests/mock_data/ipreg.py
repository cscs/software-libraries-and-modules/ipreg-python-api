"""IPReg Mock Data generator

Generates jackdaw ipreg HTTP body responses. The ipreg returns are tab delimited text responses with

example:
name\tpurpose\tremarks\treview_date\taddresses\r\n
name.com\tsomepurpose\tsomeremarks\tsomereview date\ttheaddress\r\n

"""


class IPReg:

    def __init__(self):

        self.list_subnet_headers = [
            'address',
            'mzone',
            'lan',
            'name',
            'equipment',
            'location',
            'owner',
            'end_user',
            'sysadmin',
            'remarks',
            'review_date'
        ]

        self.xlist_ops_cname_headers = [
            'name',
            'target_name',
            'purpose',
            'remarks',
            'review_date'
        ]
        self.xlist_box_list_domain_headers = [
            'name',
            'equipment',
            'location',
            'owner',
            'sysadmin',
            'end_user',
            'remarks',
            'review_date',
            'addresses'
        ]

        self.default_line_break = "\r\n"

        self.ips = [13, 15, 27, 36, 51]
        self.hostnames = ['comp123', 'comp58', 'comp66', 'comp69', 'comp89', 'comp93']
        self.equipments = ['Optiplex 7010', 'Mac Mini', 'Printer', 'Mac Mini', 'Optiplex 7010']
        self.locations = ['Level 3', 'Level 3', 'Level 1', 'My House', 'Level 1']
        self.owners = ['Finance', 'Finance', 'HR', 'Research', 'Research']
        self.users = [
            'Frank Stallone',
            'Francine Stallone',
            'Frankina Stallone',
            'Franky Stallone',
            'Sidney Applebaum'
        ]

        self.cname_targets = ['proxy01', 'proxy02', 'web01', 'web02', 'web01']

    def get_subnet_base(self, subnet):
        ip_parts = subnet.split(".")
        if len(ip_parts) != 4:
            raise Exception("Invalid IP: " + subnet)

        del (ip_parts[3])
        subnet_base = ".".join(ip_parts)
        return subnet_base

    # Load

    def load_data(self, function_call, params):
        method_to_call = getattr(self, function_call)
        result = method_to_call(**params)
        return result

    # Convenience

    def get_hostname_info(self, domain, subnet):
        return self.xlist_ops_by_list_domain(domain, subnet)

    def get_ip_info(self, subnet, domain):
        return self.list_ops_by_list_subnet(subnet, domain)

    # Xlist ops Base

    def xlist_ops_cname_by_mzone(self, mzone, domain):
        request_url = '/ipreg/xlist_ops?do_it=list_mzone&mzone=' + \
                      mzone + \
                      '&object_type=cname&record_separator=CRLF'

        cnames = []
        for i in range(0, 5):
            item = [
                self.hostnames[i] + '.' + domain,  # name
                self.cname_targets[i] + '.' + domain,  # target_name
                self.locations[i],  # purpose
                '',  # remarks
                '',  # review_data
            ]
            cnames.append(item)

        text_response = "\t".join(self.xlist_ops_cname_headers)
        text_response = text_response + self.default_line_break
        for cname in cnames:
            text_response = text_response + "\t".join(cname)
            text_response = text_response + self.default_line_break

        return {
            'url': request_url,
            'text_response': text_response
        }

    def xlist_ops_by_list_domain(self, domain, subnet):
        request_url = '/ipreg/xlist_ops?do_it=list_domain&domain=' + \
                      domain + \
                      '&object_type=box&record_separator=CRLF'

        boxes = []
        for i in range(0, 5):
            ip = self.get_subnet_base(subnet) + "." + str(self.ips[i])
            item = [
                self.hostnames[i] + '.' + domain,
                self.equipments[i],
                self.locations[i],
                self.owners[i],
                '',
                self.users[i],
                '',
                '',
                ip
            ]
            boxes.append(item)

        text_response = "\t".join(self.xlist_box_list_domain_headers)
        text_response = text_response + self.default_line_break
        for box in boxes:
            text_response = text_response + "\t".join(box)
            text_response = text_response + self.default_line_break

        return {
            'url': request_url,
            'text_response': text_response
        }

    # List ops
    def generate_list_ops_data_response(self, subnet, domain, mzone, lan):
        boxes = []
        for i in range(0, 5):
            ip = self.get_subnet_base(subnet) + "." + str(self.ips[i])
            item = [
                ip,
                mzone,
                lan,
                self.hostnames[i] + '.' + domain,
                self.equipments[i],
                self.locations[i],
                self.owners[i],
                self.users[i],
                '',
                '',
                ''
            ]
            boxes.append(item)

        text_response = "\t".join(self.list_subnet_headers)
        text_response = text_response + self.default_line_break
        for box in boxes:
            text_response = text_response + "\t".join(box)
            text_response = text_response + self.default_line_break

        return text_response

    def list_ops_by_list_mzone(self, subnet, domain, mzone, lan):
        request_url = '/ipreg/list_ops?do_it=list_mzone&mzone=' + \
                      mzone + \
                      '&record_separator=CRLF'

        text_response = self.generate_list_ops_data_response(subnet, domain, mzone, lan)

        return {
            'url': request_url,
            'text_response': text_response
        }

    def list_ops_by_list_lan(self, subnet, domain, mzone, lan):
        request_url = '/ipreg/list_ops?do_it=list_lan&lan=' + \
                      lan + \
                      '&record_separator=CRLF'

        text_response = self.generate_list_ops_data_response(subnet, domain, mzone, lan)

        return {
            'url': request_url,
            'text_response': text_response
        }

    def list_ops_by_list_domain(self, subnet, domain, mzone, lan):

        request_url = '/ipreg/list_ops?do_it=list_domain&domain=' + \
                      domain + \
                      '&record_separator=CRLF'

        text_response = self.generate_list_ops_data_response(subnet, domain, mzone, lan)

        return {
            'url': request_url,
            'text_response': text_response
        }

    def list_ops_by_list_subnet(self, subnet, domain, mzone="", lan=""):

        request_url = '/ipreg/list_ops?do_it=list_subnet&subnet_base=' + \
                      subnet + \
                      '&record_separator=CRLF'

        text_response = self.generate_list_ops_data_response(subnet, domain, mzone, lan)

        return {
            'url': request_url,
            'text_response': text_response
        }
