import tests.mock_data

mock_ipreg_data = tests.mock_data.ipreg.IPReg()

subnet = '192.168.12.0'
domain = 'mydomain.test.cam.ac.uk'

data = mock_ipreg_data.list_ops_by_list_subnet(subnet, domain)
print(data['text_response'])

data = mock_ipreg_data.xlist_ops_by_list_domain(domain, subnet)
print(data['text_response'])

# with open('ipreg--mock_list_subnet.txt', 'w') as f:
#     f.write(text_response)
