import requests
import requests_mock
import tests.mock_data

import os
import pytest

import ipreg


def test_get_ip_info_exists(ipreg_environment, get_ip_info):

    ip = get_ip_info['results']['ip']
    expected = get_ip_info['results']['expected']

    api_client = ipreg.IPRegApi(cookie_file=ipreg_environment.cookie_file_name)

    ip_info = api_client.get_ip_info(ip)

    if expected:
        assert ip_info is not None
        assert ip_info['address'] == ip
    else:
        assert ip_info is None


def test_get_domain_info(ipreg_environment, get_hostname_info):

    hostname = get_hostname_info['results']['hostname']
    expected = get_hostname_info['results']['expected']

    api_client = ipreg.IPRegApi(cookie_file=ipreg_environment.cookie_file_name)

    hostname_info = api_client.get_hostname_info(hostname)

    if expected:
        assert hostname_info is not None
        assert hostname_info['name'] == hostname
    else:
        assert hostname_info is None
