import requests
import requests_mock
import tests.mock_data

import os
import pytest

import ipreg
#
# def test_valid_string2(ipreg_environment, list_ops_by_list_subnet):
#
#     subnet = list_ops_by_list_subnet[0]
#     domain = list_ops_by_list_subnet[1]
#     expected_count = list_ops_by_list_subnet[2]
#
#     api_client = ipreg.IPRegApi(cookie_file=ipreg_environment.cookie_file_name)
#     subnet_items = api_client.get_list_ops_by_subnet(subnet)
#
#     # Check that each address that comes back from the API contains the start of the subnet
#     check_subnet = subnet.rstrip(".0")
#     for subnet_item in subnet_items:
#         assert check_subnet in subnet_item['address']
#
#     assert len(subnet_items) == expected_count
#
# def test_valid_cname(ipreg_environment, xlist_ops_cname_by_mzone):
#     mzone = xlist_ops_cname_by_mzone[0]
#     domain = xlist_ops_cname_by_mzone[1]
#     expected_count = xlist_ops_cname_by_mzone[2]
#
#
#     api_client = ipreg.IPRegApi(cookie_file=ipreg_environment.cookie_file_name)
#
#     cname_items = api_client.get_xlist_ops_by_mzone(mzone, 'cname')
#
#     # Check that each address that comes back from the API contains the start of the domain
#     for cname in cname_items:
#         assert domain in cname['name']
#         assert domain in cname['target_name']
#
#     assert len(cname_items) == expected_count
