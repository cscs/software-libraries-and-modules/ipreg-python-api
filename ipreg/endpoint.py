import requests
import os
import urllib.parse
import time
# import errno

from os import listdir
from os.path import isfile, join

import ipreg.exceptions


class Endpoint:

    def __init__(self, url_base, cookie_file, cache_dir, cache_time=180):
        self.cookie_file = cookie_file
        self.url_base = url_base

        self.load_cookie_data()

        # Make sure the time between API queries is a minimum of 5 seconds
        self.cache_dir = cache_dir

        self.cache_time = cache_time
        if self.cache_time < 5:
            self.cache_time = 5

        self.clear_cache()

    def __del__(self):
        # self.clear_cache()
        pass

    def load_cookie_data(self):
        try:
            with open(self.cookie_file, 'r') as cookie_file:
                cookie_data = cookie_file.read()
        except FileNotFoundError as e:
            raise ipreg.exceptions.JackdawCookieNotFoundError('Cookie file not found') from e

        cookie_data = cookie_data.replace('\n', '')
        cookie_data = cookie_data.replace('\t', ' ')
        cookie_name = cookie_data.split(' ')[5]
        cookie_value = cookie_data.split(' ')[6]

        self.cookie = {cookie_name: cookie_value}

    def clear_cache(self):

        cache_path = os.path.join(self.cache_dir)
        files = [f for f in listdir(cache_path) if isfile(join(cache_path, f))]
        for file in files:
            if not file == ".gitkeep":
                file = os.path.join(cache_path, file)
                os.remove(file)

    def get(self, action_op, params):

        cache_key_filename = 'ipreg--' + \
            '--'.join('{}-{}'.format(key, val) for key, val in sorted(params.items()))\
            + '.txt'
        cache_key_filename = os.path.join(self.cache_dir, cache_key_filename)

        if not self.is_cache_valid(cache_key_filename):
            try:
                action_url = urllib.parse.urljoin(self.url_base, action_op)

                r1 = requests.get(action_url, cookies=self.cookie, params=params)
                # verify='cacert.pem'
                # # windows-1252
                response = r1.content.decode('utf-8')  # 'windows-1252'

                self.write_cache(cache_key_filename, response)

            except ipreg.exceptions.CachePathNotFoundError:
                raise
            # except:
            #     # TODO: Make very specific and not catch-all
            #     raise Exception("jackdaw error")

        with open(cache_key_filename) as f:
            text_response = f.readlines()

        return text_response

    def write_cache(self, cache_key_file, data):
        try:
            with open(cache_key_file, "w") as f:
                f.write(data)
        except FileNotFoundError as e:
            raise ipreg.exceptions.CachePathNotFoundError(
                'Cache path does not exist - ' + e.filename
            )

    def is_cache_valid(self, cache_key_file):
        # As the jackdaw listops cache may be a series of separate files
        # depending on the query, we'll cache them
        # by query url key to have a cache per url

        if os.path.isfile(cache_key_file):
            last_update = os.path.getmtime(cache_key_file)
            current_time = time.time()

            if current_time - last_update < self.cache_time:
                return True

        return False
