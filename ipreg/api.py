from ipreg.endpoint import Endpoint

# TODO: Add proper exceptions and logging.


class IPRegApi:

    def __init__(self, url_base='https://jackdaw.cam.ac.uk/ipreg/',
                 cookie_file='cookie-jackdaw.txt', cache_dir='cache', cache_time=180):
        self.endpoint = Endpoint(url_base, cookie_file, cache_dir, cache_time)

        self.do_it_actions = [
            'list_mzone',
            'list_domain',
            'list_lan',
            'list_subnet'
        ]

        self.object_types = [
            'box',
            'vbox',
            'cname',
            'aname'
        ]

    def parse_response(self, text_response):

        entries = []

        # Parse headers
        headers = text_response[0].replace("\n", "").split("\t")
        del(text_response[0])

        # Parse items
        for line in text_response:
            if line == "\n":
                continue

            ip_detail = line.split("\t")
            entry = {}

            for header, index in zip(headers, range(len(headers))):
                entry[str(header).strip()] = ip_detail[index]

            entries.append(entry)

        return entries

    def get_xlist_ops(self, data_key, data_value, object_type, do_it):
        if object_type not in self.object_types:
            raise Exception('Nonono')
        if do_it not in self.do_it_actions:
            raise Exception('Nonnono2')

        action_data = {
            'do_it': do_it,
            data_key: data_value,
            'object_type': object_type,
            'record_separator': 'CRLF'
        }
        text_response = self.endpoint.get('xlist_ops', params=action_data)
        entries = self.parse_response(text_response)

        return entries

    def get_list_ops(self, data_key, data_value, do_it):
        if do_it not in self.do_it_actions:
            raise Exception('Nonnono2')

        action_data = {
            'do_it': do_it,
            data_key: data_value,
            'record_separator': 'CRLF'
        }

        text_response = self.endpoint.get('list_ops', params=action_data)
        entries = self.parse_response(text_response)

        return entries

    def get_xlist_ops_by_domain(self, domain, object_type='box'):
        return self.get_xlist_ops('domain', domain, object_type, 'list_domain')

    def get_xlist_ops_by_mzone(self, mzone, object_type='box'):
        return self.get_xlist_ops('mzone', mzone, object_type, 'list_mzone')

    def get_list_ops_by_subnet(self, subnet):
        return self.get_list_ops('subnet_base', subnet, 'list_subnet')

    def get_list_ops_by_lan(self, lan):
        return self.get_list_ops('lan', lan, 'list_lan')

    def get_list_ops_by_domain(self, domain):
        return self.get_list_ops('domain', domain, 'list_domain')

    def get_list_ops_by_mzone(self, mzone):
        return self.get_list_ops('mzone', mzone, 'list_mzone')

    def get_ip_info(self, ip):

        ip_parts = ip.split(".")
        if len(ip_parts) != 4:
            raise Exception("Invalid IP: " + ip)

        ip_parts[3] = "0"
        subnet_base = ".".join(ip_parts)

        results = self.get_list_ops_by_subnet(subnet_base)

        for item in results:
            if item['address'] == ip:
                return item

        return None

    def get_hostname_info(self, hostname):
        domain_parts = hostname.split(".")
        del(domain_parts[0])
        domain = ".".join(domain_parts)

        results = self.get_xlist_ops_by_domain(domain)
        for item in results:
            if item['name'] == hostname:
                return item

    def update_single_ops_ip(ip):
        raise NotImplementedError
