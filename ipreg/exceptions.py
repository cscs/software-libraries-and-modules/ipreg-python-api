
class JackdawCookieNotFoundError(Exception):
    """Raised when cookie not found"""
    pass


class CachePathNotFoundError(IOError):
    """Raised when cache directory not found"""
    pass
