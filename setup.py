import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pyipreg-dc677",  # Replace with your own username
    version="0.0.1",
    author="David Connor",
    author_email="dc677@cam.ac.uk",
    description="Jackdaw IPReg python API client library",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.developers.cam.ac.uk/cscs/software-libraries-and-modules/ipreg-python-api",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.2',
)
